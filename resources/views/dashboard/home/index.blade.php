<div class="page-header flex-wrap">
    <h3 class="mb-0 font-weight-bold font-25"> {{Str::ucfirst($pageTitle)}} <span class="pl-0 h6 pl-sm-2 text-muted d-inline-block"></span></h3>
</div>
