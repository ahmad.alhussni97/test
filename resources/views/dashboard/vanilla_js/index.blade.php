<button type="button" class="form-control mb-4" onclick="loadDoc()">Click For Show Data</button>

<table id="datatable" class="table is-striped">
    <thead>
    <tr>
        <th>Id</th>
        <th>Question</th>
        <th>Author</th>
        <th>Created_at</th>
        <th>Updated at</th>
    </tr>
    </thead>
    <tbody id="content">



    </tbody>
</table>


@section("section-script")
    <script>
        function loadDoc() {

            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {

                    $("#content").empty()
                    var content=''
                    var data = JSON.parse(this.responseText)

                    for (var i = 0; i < data.length; i++){
                        content +=' <tr>'
                        content +=' <th scope="row">'+data[i]["id"]+'</th>'
                        content +=' <td scope="row">'+ data[i]["question"].split(/\s+/).slice(0, 10).join(" ")+'</td>'
                        content +=' <td scope="row">'+data[i]["author"]+'</td>'
                        content +=' <td scope="row">'+data[i]["created_at"]+'</td>'
                        content +=' <td scope="row">'+data[i]["updated_at"]+'</td>'
                        content +=' <tr>'
                        $("#content").append(content)
                    }

                }
            };

            xhttp.open("GET", "https://my.api.mockaroo.com/question_api?key=95090cb0", true);
            xhttp.send();
        }
    </script>
@endsection












