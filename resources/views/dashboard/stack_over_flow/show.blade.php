<div class="card">
    <div class="card-body">
        <h4 class="card-title">{{$pageTitle}}</h4>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group row">
                    {{ Form::label('id','Id :',array('class' => 'col-md-3 col-form-label')) }}
                    <div class="col-md-9">
                        {{ Form::text('id',$data["id"]??'',array('class' => 'form-control','readonly'=>true)) }}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-9">
                <div class="form-group row">
                    {{ Form::label('question','Question :',array('class' => 'col-md-2 col-form-label')) }}
                    <div class="col-md-9">
                        {!! Form::textarea('description',$data["question"]??'', ['class'=>'form-control',"id"=>"description",'readonly'=>true]) !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group row">
                    {{ Form::label('author','Author :',array('class' => 'col-md-3 col-form-label')) }}
                    <div class="col-md-9">
                        {{ Form::text('author',$data["author"]??'',array('class' => 'form-control','readonly'=>true)) }}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group row">
                    {{ Form::label('Created At','Created At :',array('class' => 'col-md-3 col-form-label')) }}
                    <div class="col-md-9">
                        {{ Form::text('created_at',$data["created_at"]??'',array('class' => 'form-control','readonly'=>true)) }}
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-6">
                <div class="form-group row">
                    {{ Form::label('Updated At','Updated At :',array('class' => 'col-md-3 col-form-label')) }}
                    <div class="col-md-9">
                        {{ Form::text('updated_at',$data["updated_at"]??'',array('class' => 'form-control','readonly'=>true)) }}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group row" style="background:#0D462E;color:white">
                    {{ Form::label('answer details','Answer Details',array('class' => 'col-md-12 col-form-label')) }}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group row">
                    {{ Form::label('id','Id :',array('class' => 'col-md-3 col-form-label')) }}
                    <div class="col-md-9">
                        {{ Form::text('id',$data["answer"]["id"]??'',array('class' => 'form-control','readonly'=>true)) }}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group row">
                    {{ Form::label('author','Author :',array('class' => 'col-md-3 col-form-label')) }}
                    <div class="col-md-9">
                        {{ Form::text('user',$data["answer"]["user"]??'',array('class' => 'form-control','readonly'=>true)) }}
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-9">
                <div class="form-group row">
                    {{ Form::label('answer','Answer :',array('class' => 'col-md-2 col-form-label')) }}
                    <div class="col-md-9">
                        {!! Form::textarea('text',$data["answer"]["text"]??'', ['class'=>'form-control',"id"=>"description",'readonly'=>true]) !!}
                    </div>
                </div>
            </div>
        </div>

        <a class="btn btn-light " href="{{ route('stack_over_flow.index') }}">Cancel</a>
    </div>
</div>
