<table id="datatable" class="table is-striped">
    <thead>
    <tr>
        <th>Id</th>
        <th>Question</th>
        <th>Author</th>
        <th>Created_at</th>
        <th>Updated at</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    @if(isset($data["response"][0]) && !empty($data["response"][0]))
        @foreach($data["response"] as $respo)
            <tr>
                <th scope="row">{{$respo["id"]??''}}</th>
                <td>{{implode(' ',array_slice(explode(' ',$respo["question"])??'',0,10))}}</td>
                <td>{{$respo["author"]??''}}</td>
                <td>{{$respo["created_at"]??''}}</td>
                <td>{{$respo["updated_at"]??''}}</td>
                <td><a href="{{route('stack_over_flow.showData',["id"=>$respo["id"]])}}"> Show Data </a></td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>









