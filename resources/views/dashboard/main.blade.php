@extends('layouts.app')
@section('content')
    <div class="content-wrapper pb-0">
        <!-- Main content-->
        @include("dashboard.".$class.".".$function,["pageTitle"=>$pageTitle,"data"=>$data??''])
        <!-- End main content -->
    </div>
@endsection
