<x-dashboard.create-widget :pageTitle="$pageTitle" :route="route('author.create')" :address="'Add Author'" />

<table id="datatable" class="table is-striped">
    <thead>
    <tr>
        <th>No</th>
        <th>Id</th>
        <th>Name</th>
        <th>Summary</th>
        <th>Educational Degree</th>
        <th>Created_at</th>
        <th>Updated at</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>


@section("section-script")
    <script type="text/javascript">
        $(document).ready(function () {
            var table = '';
            $(function () {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                table = $('#datatable').DataTable({
                    scrollY: '60vh',
                    scrollCollapse: true,
                    buttons: [
                        {
                            extend: 'csv',
                            split: ['colvis', 'copy', 'csv', 'excel', 'print', 'pdf'],
                        }
                    ],
                    lengthMenu: [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "All"]],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    autoWidth: true,
                    order: [],
                    ajax: {
                        "url": "{{ route('author.datatable') }}",
                        "type": "POST",
                        "cache": false,
                        "data": function (d) {
                            // d.is_active = $('.users').val();
                        }
                    },
                    columns: [
                        {data: 'DT_RowIndex', orderable: false, searchable: false},
                        {data: 'id'},
                        {data: 'name'},
                        {data: 'summary'},
                        {data: 'educational_degree'},
                        {
                            data: 'created_at',
                            render: function (data) {
                                if (data) {
                                    var date = new Date(data);
                                    var day = (date.getDate() < 10 ? '0' : '') + date.getDate();
                                    var month = (date.getMonth() < 9 ? '0' : '') + (date.getMonth() + 1);
                                    var year = date.getFullYear();
                                    var hours = ("0" + date.getHours()).slice(-2);
                                    var minutes = ("0" + date.getMinutes()).slice(-2);
                                    var seconds = ("0" + date.getSeconds()).slice(-2);
                                    var formattedDate = year + '-' + month + '-' + day + ' ' + hours + ':' + minutes + ':' + seconds;
                                    return formattedDate;
                                }
                                return '';
                            }
                        },
                        {
                            data: 'updated_at',
                            render: function (data) {
                                if (data) {
                                    var date = new Date(data);
                                    var day = (date.getDate() < 10 ? '0' : '') + date.getDate();
                                    var month = (date.getMonth() < 9 ? '0' : '') + (date.getMonth() + 1);
                                    var year = date.getFullYear();
                                    var hours = ("0" + date.getHours()).slice(-2);
                                    var minutes = ("0" + date.getMinutes()).slice(-2);
                                    var seconds = ("0" + date.getSeconds()).slice(-2);
                                    var formattedDate = year + '-' + month + '-' + day + ' ' + hours + ':' + minutes + ':' + seconds;
                                    return formattedDate;
                                }
                                return '';
                            }
                        }
                    ]
                });
            });
        });
    </script>
@endsection









