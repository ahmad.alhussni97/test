<div class="card">
    <div class="card-body">
        <h4 class="card-title">{{$pageTitle}}</h4>
        {{ Form::open(array('url' => route('author.store'),'method'=>'POST','class'=>'form-sample')) }}
        @csrf
        <p class="card-description">Book info</p>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group row">
                    {{ Form::label('name','Name ('.getCurrentLang().') :',array('class' => 'col-md-3 col-form-label')) }}
                    <div class="col-md-9">
                        {{ Form::text('name',old('name')??'',array('class' => 'form-control','placeholder' => 'Enter name','required'=>true)) }}
                        @error('name')
                        <x-dashboard.error-form :message="$message"/>
                        @enderror
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-7">
                <div class="form-group row">
                    {{ Form::label('select_educational_degree','Select educational degree',array('class' => 'col-md-3 col-form-label')) }}
                    <div class="col-md-9">
                        <select class="selectpicker" data-size="5" name="educational_degree" data-live-search="true" style="width: 100%;">
                                <option value=""></option>
                                <option value="Associate's Degree" @php if(old("educational_degree")=="Associate's Degree") echo "selected"; @endphp>Associate's Degree</option>
                                <option value="Bachelor's Degree" @php if(old("educational_degree")=="Bachelor's Degree") echo "selected"; @endphp>Bachelor's Degree</option>
                                <option value="Master's Degree" @php if(old("educational_degree")=="Master's Degree") echo "selected"; @endphp>Master's Degree</option>
                                <option value="Doctorate" @php if(old("educational_degree")=="Doctorate") echo "selected"; @endphp>Doctorate</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group row">
                    {{ Form::label('summary','Summary :',array('class' => 'col-md-3 col-form-label')) }}
                    <div class="col-md-11">
                        {!! Form::textarea('summary',old('summary')??'', ['class'=>'form-control',"id"=>"description"]) !!}
                    </div>
                </div>
            </div>
        </div>

        {{Form::submit('Submit',array('class'=>'btn btn-primary mr-2'))}}
        <a class="btn btn-light " href="{{ route('author.index') }}">Cancel</a>
        {{ Form::close() }}
    </div>
</div>

@section("section-script")
<script>
    $(document).ready(function () {

        $(function () {
            $('.selectpicker').addClass("form-control width-100");
        });

        var editor = new FroalaEditor('#description',
            {
                fontFamily: {
                    'Bon Vivant Family Serif, Bold': 'Font 1'
                }
            });
    });

</script>
@endsection
