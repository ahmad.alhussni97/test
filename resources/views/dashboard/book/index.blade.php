<x-dashboard.create-widget :pageTitle="$pageTitle" :route="route('book.create')" :address="'Add Book'"/>
<div class="container">
    <div class="row">
        <div class="col-md-3 my-2 my-md-0">
            <div class="d-flex align-items-center">
                <select class="form-control selectpicker main_category mb-5" name="main_category"
                        id="main_category" title="All Main Category">
                    <option value="">All Main Category</option>
                    @foreach($data["mainCategory"] as $main)
                        <option value="{{ $main->id }}">{{ $main->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3 my-2 my-md-0">
            <div class="d-flex align-items-center">
                <select class="form-control selectpicker sub_category mb-5" name="sub_category"
                        id="sub_category" title="All Sub Category">
                    <option value="">All Sub Category</option>
                </select>
            </div>
        </div>
    </div>
</div>


<table id="datatable" class="table is-striped">
    <thead>
    <tr>
        <th>No</th>
        <th>Id</th>
        <th>Name</th>
        <th>Summary</th>
        <th>Page_count</th>
        <th>Category</th>
        <th>Created_at</th>
        <th>Updated at</th>
        <th>Content</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>


@section("section-script")
    <script type="text/javascript">
        $(document).ready(function () {
            var table = '';
            $(function () {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                table = $('#datatable').DataTable({
                    scrollY: '60vh',
                    scrollCollapse: true,
                    buttons: [
                        {
                            extend: 'csv',
                            split: ['colvis', 'copy', 'csv', 'excel', 'print', 'pdf'],
                        }
                    ],
                    lengthMenu: [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "All"]],
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    autoWidth: true,
                    order: [],
                    ajax: {
                        "url": "{{ route('book.datatable') }}",
                        "type": "POST",
                        "cache": false,
                        "data": function (d) {
                            d.main_category = $("#main_category").val()
                            d.sub_category = $("#sub_category").val()
                        }
                    },
                    columns: [
                        {data: 'DT_RowIndex', orderable: false, searchable: false},
                        {data: 'id'},
                        {data: 'name'},
                        {data: 'summary'},
                        {data: 'page_count'},
                        {data: 'category'},
                        {
                            data: 'created_at',
                            render: function (data) {
                                if (data) {
                                    var date = new Date(data);
                                    var day = (date.getDate() < 10 ? '0' : '') + date.getDate();
                                    var month = (date.getMonth() < 9 ? '0' : '') + (date.getMonth() + 1);
                                    var year = date.getFullYear();
                                    var hours = ("0" + date.getHours()).slice(-2);
                                    var minutes = ("0" + date.getMinutes()).slice(-2);
                                    var seconds = ("0" + date.getSeconds()).slice(-2);
                                    var formattedDate = year + '-' + month + '-' + day + ' ' + hours + ':' + minutes + ':' + seconds;
                                    return formattedDate;
                                }
                                return '';
                            }
                        },
                        {
                            data: 'updated_at',
                            render: function (data) {
                                if (data) {
                                    var date = new Date(data);
                                    var day = (date.getDate() < 10 ? '0' : '') + date.getDate();
                                    var month = (date.getMonth() < 9 ? '0' : '') + (date.getMonth() + 1);
                                    var year = date.getFullYear();
                                    var hours = ("0" + date.getHours()).slice(-2);
                                    var minutes = ("0" + date.getMinutes()).slice(-2);
                                    var seconds = ("0" + date.getSeconds()).slice(-2);
                                    var formattedDate = year + '-' + month + '-' + day + ' ' + hours + ':' + minutes + ':' + seconds;
                                    return formattedDate;
                                }
                                return '';
                            }
                        },
                        {
                            data: 'action', searchable: false, orderable: false
                        }
                    ]
                });
            });


            $('.main_category').change(function () {

                // set main and sub category
                $("#sub_category").empty()
                $("#sub_category").append('<option value="">All Sub Category</option>')

                $.ajax({
                    method:"POST",
                    url:'<?=route("book.getSubCategory") ?>',
                    cache: false,
                    data:{"main_category":$("#main_category").val()},
                    success: function(html){
                        $("#sub_category").append(html.result);
                    },error: function(xhr, ajaxOptions, thrownError){
                        console.log(xhr.status)
                    }
                });

                 // reload table
                table.ajax.reload(null, false);
            });


            $('.sub_category').change(function () {
                table.ajax.reload(null, false);
            });

        });

    </script>
@endsection









