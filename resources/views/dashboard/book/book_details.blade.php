<div class="card">
    <div class="card-body">
        <h4 class="card-title">{{$pageTitle}}</h4>
        {{ Form::open(array('url' => route('book_details.store'),'method'=>'POST','class'=>'form-sample')) }}
        @csrf
        <div class="row">
            <div class="col-md-10">
                <div class="form-group row">
                    <div class="col-md-4">
                        <input name="book_id" value="{{$book_id}}" type="hidden">
                    </div>
                </div>
            </div>
        </div>
        <div id="details">
            <div class="form-group row mb-0">
                <div data-repeater-list="details">
                    <div data-repeater-item>
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        {{ Form::number('page_id','',array('class' => 'form-control','placeholder' => 'Enter Page Number','required'=>true)) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group row">
                                    {{ Form::label('content','Content :',array('class' => 'col-md-3 col-form-label')) }}
                                    <div class="col-md-11">
                                        {!! Form::textarea('content','', ['class'=>'form-control',"id"=>"description"]) !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="form-group mb-5">
                    <a href="javascript:;" data-repeater-create class="btn btn-light-primary">
                        <i class="fa fa-plus mr-2"></i>Add
                    </a>
                </div>
            </div>
        </div>

        {{Form::submit('Submit',array('class'=>'btn btn-primary mr-2'))}}
        <a class="btn btn-light " href="{{ route('book.index') }}">Cancel</a>
        {{ Form::close() }}
    </div>
</div>

@section("section-script")
    <script>
        $(document).ready(function () {
            $(function () {
                $('.selectpicker').addClass("form-control width-100");
            });
        });
        $('#details').repeater({
            initEmpty: false,

            defaultValues: {},

            show: function () {
                $(this).slideDown();
                new FroalaEditor('#description',
                    {
                        fontFamily: {
                            'Bon Vivant Family Serif, Bold': 'Font 1'
                        }
                    });
            },

            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            },

            ready: function () {
                new FroalaEditor('#description',
                    {
                        fontFamily: {
                            'Bon Vivant Family Serif, Bold': 'Font 1'
                        }
                    });
            }
        });
    </script>
@endsection

