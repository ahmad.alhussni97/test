<div class="card">
    <div class="card-body">
        <h4 class="card-title">{{$pageTitle}}</h4>
        {{ Form::open(array('url' => route('book.store'),'method'=>'POST','class'=>'form-sample')) }}
        @csrf
        <p class="card-description">Book info</p>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group row">
                    {{ Form::label('name','Name ('.getCurrentLang().') :',array('class' => 'col-md-3 col-form-label')) }}
                    <div class="col-md-9">
                        {{ Form::text('name',old('name')??'',array('class' => 'form-control','placeholder' => 'Enter name','required'=>true)) }}
                        @error('name')
                        <x-dashboard.error-form :message="$message"/>
                        @enderror
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-7">
                <div class="form-group row">
                    {{ Form::label('select_author','Select author',array('class' => 'col-md-3 col-form-label')) }}
                    <div class="col-md-9">
                        <select class="selectpicker" data-size="5" name="author_id" data-live-search="true" style="width: 100%;" multiple required>
                            @if(isset($data["authors"]) &&  checkVariable($data["authors"]))
                                <option value=""></option>
                                @foreach($data["authors"] as $author)
                                    <option
                                        value="{{$author->id}}" @php if(old("category_id")==$author->id) echo "selected"; @endphp>
                                        {{ucwords($author->name??trans('general.not_found'))}}
                                    </option>
                                @endforeach
                            @endif
                        </select>
                        @error('author_id')
                        <x-dashboard.error-form :message="$message"/>
                        @enderror
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-7">
                <div class="form-group row">
                    {{ Form::label('select_category','Select category',array('class' => 'col-md-3 col-form-label')) }}
                    <div class="col-md-9">
                        <select class="selectpicker" data-size="5" name="cat_id" data-live-search="true" style="width: 100%;">
                            @if(isset($data["categories"]) &&  checkVariable($data["categories"]))
                            <option value=""></option>
                            @foreach($data["categories"] as $category)
                            <option
                                value="{{$category->id}}" @php if(old("category_id")==$category->id) echo "selected"; @endphp>
                            {{ucwords($category->name??trans('general.not_found'))}}
                            </option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group row">
                    {{ Form::label('page_count','Page_count :',array('class' => 'col-md-3 col-form-label')) }}
                    <div class="col-md-5">
                        {!! Form::number('page_count',old('page_count')??'', ['class'=>'form-control']) !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group row">
                    {{ Form::label('summary','Summary :',array('class' => 'col-md-3 col-form-label')) }}
                    <div class="col-md-11">
                        {!! Form::textarea('summary',old('summary')??'', ['class'=>'form-control',"id"=>"description"]) !!}
                    </div>
                </div>
            </div>
        </div>

        {{Form::submit('Submit',array('class'=>'btn btn-primary mr-2'))}}
        <a class="btn btn-light " href="{{ route('book.index') }}">Cancel</a>
        {{ Form::close() }}
    </div>
</div>

@section("section-script")
<script>
    $(document).ready(function () {
        $(function () {
            $('.selectpicker').addClass("form-control width-100");
        });

        var editor = new FroalaEditor('#description',
            {
                fontFamily: {
                    'Bon Vivant Family Serif, Bold': 'Font 1'
                }
            });
    });

</script>
@endsection
