<div class="card">
    <div class="card-body">
        <h4 class="card-title">{{$pageTitle}}</h4>
        @csrf
        <div class="row">
            <div class="col-md-10">
                <div class="form-group row">
                    <div class="col-md-4">
                        <input name="book_id" value="{{$book_id}}" type="hidden">
                    </div>
                </div>
            </div>
        </div>
        <div id="details">
            <div>
                <div data-repeater-list="details">
                    <div data-repeater-item>
                        @foreach($data["bookDetails"] as $bookDetails)
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    {{ Form::label('page_id','Page Id :',array('class' => 'col-md-6 col-form-label')) }}
                                    <div class="col-md-6">
                                        {{ Form::number('page_id',$bookDetails->page_id??'',array('class' => 'form-control','placeholder' => 'Enter Page Number','readonly'=>true)) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group row">
                                    {{ Form::label('content','Content :',array('class' => 'col-md-3 col-form-label')) }}
                                    <div class="col-md-11">
                                        {{ strip_tags($bookDetails->content)}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <a class="btn btn-light " href="{{ route('book.index') }}">Cancel</a>
    </div>
</div>

