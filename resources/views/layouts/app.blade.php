<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Leaders translation') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">

    <!-- dashboard design -->
    @auth
        <link rel="stylesheet" href="{{asset("assets/dashboard/vendors/mdi/css/materialdesignicons.min.css")}}"/>
        <link rel="stylesheet" href="{{asset("assets/dashboard/vendors/flag-icon-css/css/flag-icon.min.css")}}"/>
        <link rel="stylesheet" href="{{asset("assets/dashboard/vendors/css/vendor.bundle.base.css")}}"/>
        <link rel="stylesheet" href="{{asset("assets/dashboard/vendors/font-awesome/css/font-awesome.min.css")}}"/>
        <link rel="stylesheet" href="{{asset("assets/dashboard/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css")}}"/>
        <link rel="stylesheet" href="{{asset("assets/dashboard/css/style.css")}}"/>

        @if(isset($function) && checkVariable($function,"index"))
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.9.3/css/bulma.min.css"/>
            <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/dataTables.bulma.min.css"/>
        @endif

        @if(isset($function) && !(checkVariable($function,"index")))
            <link rel="stylesheet" href="{{asset("assets/dashboard/vendors/select2/select2.min.css")}}"/>
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
            <link href='https://cdn.jsdelivr.net/npm/froala-editor@latest/css/froala_editor.pkgd.min.css' rel='stylesheet' type='text/css' />
        @endif

        <link rel="stylesheet" href="{{asset("assets/dashboard/css/style-custom.css")}}"/>
    @endauth
    <!-- End dashboard design -->

</head>
<body>
<div id="app">

    @guest
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm mb-4">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Leaders translation') }}
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav me-auto">

                    </ul>
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ms-auto">
                        <!-- Authentication Links -->

                        @if (Route::has('login'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                        @endif

                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif

                    </ul>
                </div>
            </div>
        </nav>
        @yield('content')
    @endguest

    @auth

        @php
            $websiteInfo=getWebsiteInfo();
            $userInfo=getUserInfo();
            $menu=getMenusDashboard();
        @endphp

        <div class="container-scroller">
            <!--Sidebar-->
            <x-dashboard.side-bar :websiteInfo="$websiteInfo" :userInfo="$userInfo" :menu="$menu"/>
            <!--End sidebar-->
            <div class="container-fluid page-body-wrapper">
                <!--Navbar-->
                <x-dashboard.navbar :userInfo="$userInfo"/>
                <!--End navbar-->
                <!--Main body -->
                <div class="main-panel">
                    @yield('content')
                </div>
                <!-- End body-->
            </div>
        </div>

        <script src="{{asset("assets/dashboard/vendors/js/vendor.bundle.base.js")}}"></script>
        <script src="{{asset("assets/dashboard/vendors/typeahead.js/typeahead.bundle.min.js")}}"></script>
        <script src="{{asset("assets/dashboard/vendors/chart.js/Chart.min.js")}}"></script>
        <script src="{{asset("assets/dashboard/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js")}}"></script>
        <script src="{{asset("assets/dashboard/vendors/flot/jquery.flot.js")}}"></script>
        <script src="{{asset("assets/dashboard/vendors/flot/jquery.flot.resize.js")}}"></script>
        <script src="{{asset("assets/dashboard/vendors/flot/jquery.flot.categories.js")}}"></script>
        <script src="{{asset("assets/dashboard/vendors/flot/jquery.flot.fillbetween.js")}}"></script>
        <script src="{{asset("assets/dashboard/vendors/flot/jquery.flot.stack.js")}}"></script>
        <script src="{{asset("assets/dashboard/vendors/flot/jquery.flot.pie.js")}}"></script>
        <script src="{{asset("assets/dashboard/js/off-canvas.js")}}"></script>
        <script src="{{asset("assets/dashboard/js/hoverable-collapse.js")}}"></script>
        <script src="{{asset("assets/dashboard/js/misc.js")}}"></script>
        <script src="{{asset("assets/dashboard/js/custom.js")}}"></script>

        @if(isset($function) && checkVariable($function,"index"))
            <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
            <script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
            <script src="https://cdn.datatables.net/1.13.1/js/dataTables.bulma.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
            <script src="https://cdn.tutorialjinni.com/notify/0.4.2/notify.min.js"></script>
        @endif

        @if(isset($function) && !(checkVariable($function,"index")))
            <script src="{{asset("assets/dashboard/vendors/select2/select2.min.js")}}"></script>
            <script src="{{asset("assets/dashboard/js/file-upload.js")}}"></script>
            <script src="{{asset("assets/dashboard/js/select2.js")}}"></script>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/i18n/defaults-*.min.js"></script>
            <script src="{{asset("assets/dashboard/js/jquery.repeater.js")}}"></script>
            <script type='text/javascript' src='https://cdn.jsdelivr.net/npm/froala-editor@latest/js/froala_editor.pkgd.min.js'></script>
            <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
            <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
            <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
        @endif

        @yield("section-script")

    @endauth
</div>
</body>
</html>
