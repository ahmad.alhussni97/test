<div class="page-header flex-wrap">
    <h1 class="mb-0 font-weight-bold font-25"> {{Str::ucfirst($pageTitle)}} <span
            class="pl-0 h6 pl-sm-2 text-muted d-inline-block"></span></h1>

    <div class="d-flex">
        <a href="{{$route}}" class="btn btn-primary btn-fw"> {{$address}} <i
                class="mdi mdi-plus btn-icon-append"></i></a>
    </div>

</div>
