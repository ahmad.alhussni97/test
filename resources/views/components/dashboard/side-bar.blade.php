<nav class="sidebar sidebar-offcanvas" id="sidebar">
    {{--    <div class="text-center sidebar-brand-wrapper d-flex align-items-center">--}}
    {{--        <a class="sidebar-brand brand-logo" href="{{route("dashboard")}}"><img--}}
    {{--                src="{{$websiteInfo->logo}}" alt="logo"/></a>--}}
    {{--        <a class="sidebar-brand brand-logo-mini pl-4 pt-3" href="{{route("dashboard")}}"><img--}}
    {{--                src="{{$websiteInfo->logoMini}}" alt="logo"/></a>--}}
    {{--    </div>--}}
    <ul class="nav">
        <li class="nav-item nav-profile">
            <a href="#" class="nav-link">
                <div class="nav-profile-image">
                    <img src="{{$userInfo->img}}" alt="profile"/>
                    <span class="login-status online"></span>
                </div>
                <div class="nav-profile-text d-flex flex-column pr-3">
                    <span class="font-weight-medium mb-2 font-weight-bold">{{Str::ucfirst($userInfo->name)}}</span>
                    <span class="font-weight-normal color-green role-text">{{$userInfo->role}}</span>
                </div>
            </a>
        </li>
        <!--menu-->
        @foreach($menu as $key=>$m)
            @if(isset($m["sub_menu"]) && checkVariable($m["sub_menu"]))
                <li class="nav-item">
                    <a class="nav-link" data-toggle="collapse" href="#ui-basic-{{$key}}" aria-expanded="false"
                       aria-controls="ui-basic"><i class="{{$m["icon"]}}  menu-icon"></i>
                        <span class="menu-title">{{$m["name"]}}</span><i class="menu-arrow"></i>
                    </a>
                    <div class="collapse" id="ui-basic-{{$key}}">
                        <ul class="nav flex-column sub-menu">
                            @foreach($m["sub_menu"] as $sub_menu)
                                <li class="nav-item">
                                    <a class="nav-link" href="{{$sub_menu["url"]}}">{{$sub_menu["name"]}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </li>
            @else
                <li class="nav-item">
                    <a class="nav-link" href="{{$m["url"]}}">
                        <i class="{{$m["icon"]}}  menu-icon"></i>
                        <span class="menu-title">{{$m["name"]}}</span>
                    </a>
                </li>
        @endif
    @endforeach
    <!--End menu -->
    </ul>
</nav>
