<div class="container">
    <div class="row p-2">
        <nav class="nav nav-pills flex-column flex-sm-row">
            <ul class="nav nav-pills nav-pills-lang" id="lang">
                @foreach(getLanguages() as $key=>$val)
                    <li class="nav-item">
                        <button @php if($page=="form") { @endphp onclick="window.location.href='{{url()->current()."?lang=".$val}}'"   @php  } @endphp  class="nav-link nav-link-lang color-black font-weight-bold {{(app()->getLocale()==$val)? 'lang-active' : ''}}" id="{{$val}}">{{$key}}</button>
                    </li>
                @endforeach
            </ul>
        </nav>
    </div>
</div>
