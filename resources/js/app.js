import './bootstrap';
import '../scss/layout.scss'
import '../scss/home.scss'


$(document).ready(function () {

    // Mobile Navbar
    $(".mobile-nav").click(function () {

        if ($(".nav").hasClass("hidden-mobile"))
            $(".nav").removeClass("hidden-mobile")
        else
            $(".nav").addClass("hidden-mobile")

    });


    // Faq Collapse
    $(".faq-title").click(function () {

        if ($(this).find("a").hasClass("collapsed"))
            $(this).find("i").addClass("fa-plus").removeClass("fa-minus");
        else
            $(this).find("i").addClass("fa-minus").removeClass("fa-plus");

    });

});
