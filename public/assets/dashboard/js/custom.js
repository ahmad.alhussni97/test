function removeItem(table,data) {

    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.isConfirmed) {
            $(this).removeClass("delete-item");
            $(this).prop("onclick", null).off("click");
            $.ajax({
                type: "DELETE",
                url: data.url,
                cache: false,
                data: {"_token": data.csrf_token},
                success: function (data) {
                    if (data.code > 0) {
                        Swal.fire(
                            data.msg
                        );
                        $.notify(data.msg, {
                            allow_dismiss: false,
                            type: 'success',
                            showProgressbar: true
                        });

                        table.ajax.reload(null, false);

                    } else {
                        $.notify(data.msg, {
                            allow_dismiss: false,
                            type: 'danger',
                            showProgressbar: true
                        });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $.notify('General Error', {
                        allow_dismiss: false,
                        type: 'danger',
                        showProgressbar: true
                    });
                }
            });
        }
    });

}







