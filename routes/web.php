<?php

use App\Http\Controllers\Dashboard\DashboardController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Dashboard\BookController;
use App\Http\Controllers\Dashboard\AuthorController;
use App\Http\Controllers\Dashboard\StackOverFlowController;
use App\Http\Controllers\Dashboard\VanillaJSController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return redirect()->route("login");
});

Auth::routes();

Route::prefix('dashboard')->group(function () {

    // check auth , permission users for all dashboard
    Route::middleware(['auth'])->group(function () {

        Route::get('/', [DashboardController::class, 'index'])->name('dashboard');

        //book route
        Route::prefix("book")->group(function (){
            Route::get('/', [BookController::class, 'index'])->name('book.index');
            Route::post('/datatable', [BookController::class, 'datatable'])->name('book.datatable');

            Route::get('/create', [BookController::class, 'create'])->name('book.create');
            Route::post('/store', [BookController::class, 'store'])->name('book.store');

            Route::post('/get_sub_category', [BookController::class, 'getSubCategory'])->name('book.getSubCategory');
        });


        Route::prefix("book_details")->group(function (){
            Route::get('/create_book_details/{book_id}', [BookController::class, 'createDetails'])->name('book_details.create');
            Route::post('/store_book_details', [BookController::class, 'storeDetails'])->name('book_details.store');

            Route::get('/show_book_details/{book_id}', [BookController::class, 'show'])->name('book_details.show');
        });

        //author route
        Route::prefix("author")->group(function (){
            Route::get('/', [AuthorController::class, 'index'])->name('author.index');
            Route::post('/datatable', [AuthorController::class, 'datatable'])->name('author.datatable');

            Route::get('/create', [AuthorController::class, 'create'])->name('author.create');
            Route::post('/store', [AuthorController::class, 'store'])->name('author.store');
        });


        //StackOverFlow
        Route::prefix("stack_over_flow")->group(function (){
            Route::get('/', [StackOverFlowController::class, 'index'])->name('stack_over_flow.index');
            Route::get('/show_data/{id}', [StackOverFlowController::class, 'showData'])->name('stack_over_flow.showData');
        });

        //StackOverFlow
        Route::prefix("vanilla_js")->group(function (){
            Route::get('/', [VanillaJSController::class, 'index'])->name('vanilla_js.index');
        });

    });
});




