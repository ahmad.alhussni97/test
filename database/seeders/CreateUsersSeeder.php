<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class CreateUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'super_admin',
                'email' => 'super_admin@gmail.com',
                'password' => bcrypt('12345678')
            ],
            [
                'name' => 'admin',
                'email' => 'admin@gmail.com',
                'password' => bcrypt('12345678')
            ],
            [
                'name' => 'user',
                'email' => 'user@gmail.com',
                'password' => bcrypt('12345678')
            ]
        ];

        DB::beginTransaction();

        foreach ($users as $user) {

            $user = User::create($user);

            $role = Role::create(['name' => $user['name']]);

            // User Role For dashboard
            if ($user['name'] != 'user') {
                $permissions = Permission::pluck('id', 'id')->all();

                $role->syncPermissions($permissions);
            }

            $user->assignRole([$role->id]);
        }

        DB::commit();

    }
}
