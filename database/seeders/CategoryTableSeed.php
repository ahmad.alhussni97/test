<?php

namespace Database\Seeders;

use App\Models\BookCategory;
use Illuminate\Database\Seeder;

class CategoryTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories= [
            [
                "id" => 1,
                "name" => "Science",
                "parent_id" => null
            ],
            [
                "id" => 2,
                "name" => "Bring_out_the_animals",
                "parent_id" => null
            ],
            [
                "id" => 3,
                "name" => "Software Engineering",
                "parent_id" => 1
            ],
            [
                "id" => 4,
                "name" => "medicine",
                "parent_id" => 1
            ]
        ];

        for ($i = 0; $i < count($categories); $i++) {
            BookCategory::create($categories[$i]);
        }

    }
}
