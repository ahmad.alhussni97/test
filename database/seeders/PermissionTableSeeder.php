<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'access_dashboard',
            'access_profile',
            'show-role',
            'create-role',
            'update-role',
            'delete-role',
            'show-user',
            'create-user',
            'update-user',
            'delete-user',
            'show-menu',
            'create-menu',
            'update-menu',
            'delete-menu',
            'show-service_main_page',
            'update-service_main_page',
            'show-service',
            'create-service',
            'update-service',
            'delete-service',
            'show-language',
            'create-language',
            'update-language',
            'delete-language',
            'show-service-form',
            'show-about-us',
            'update-about-us',
            'show-faq',
            'create-faq',
            'update-faq',
            'delete-faq',
            'show-privacy-and-policy',
            'create-privacy-and-policy',
            'update-privacy-and-policy',
            'delete-privacy-and-policy',
            'show-term-of-use',
            'create-term-of-use',
            'update-term-of-use',
            'delete-term-of-use',
            'show-client',
            'create-client',
            'update-client',
            'delete-client',
            'show-client-category',
            'create-client-category',
            'update-client-category',
            'delete-client-category',
            'show-blog',
            'create-blog',
            'update-blog',
            'delete-blog',
            'show-blog-category',
            'create-blog-category',
            'update-blog-category',
            'delete-blog-category',
            'show-contact-us-info',
            'update-contact-us-info',
            'show-contact-us-form',
            'show-branch',
            'update-branch',
            'show-headline',
            'update-headline',
            'show-career_main_page',
            'update-career_main_page',
            'show-career',
            'create-career',
            'update-career',
            'delete-career',
            'show-department',
            'update-department',
            'show-career-form',
            'download-career-cv',
            'show-home_page',
            'update-home_page',
            'show-setting',
            'update-setting',
            'show-get_a_quote_page',
            'update-get_a_quote_page',
            'show-get_a_quote-form',
        ];

        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }
    }
}
