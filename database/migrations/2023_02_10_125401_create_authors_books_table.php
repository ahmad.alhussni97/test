<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('authors_books', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("author_id")->nullable();
            $table->unsignedBigInteger("book_id")->nullable();

            $table->foreign('author_id')->references('id')->on('authors')
                ->onDelete('CASCADE')->onUpdate('CASCADE');

            $table->foreign('book_id')->references('id')->on('books')
                ->onDelete('CASCADE')->onUpdate('CASCADE');

            $table->unique(['author_id', 'book_id']);
            $table->index(['author_id', 'book_id']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('authors_books');
    }
};
