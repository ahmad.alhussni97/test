<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_details', function (Blueprint $table) {
            $table->id();
            $table->integer("page_id");
            $table->text("content")->nullable();
            $table->unsignedBigInteger("book_id")->nullable();
            $table->foreign('book_id')->references('id')->on('books')
                ->onDelete('CASCADE')->onUpdate('CASCADE');

            $table->index(['page_id', 'book_id']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_details');
    }
};
