<?php

return [
    'not_found' => "غير محدد",
    'faq' => "الاسئلة الشائعة",
    'privacy&policy' => "السياسة والخصوصية",
    'terms_of_use' => "المصطلحات المستخدمة",
    'all' => "الكل"
];
