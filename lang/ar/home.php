<?php

return [
    "we_speak_your_language" => "نحن نتحدث لغتك",
    "here_you_go" => "ها أنت ذا",
    "what_we_promise_you" => "ما الذي تعدك به",
    "what_makes_us_different" => "ما الذي يجعلنا مختلفين",
    "unique_translation" => "ترجمة فريدة",
    "distinctive_interpretation" => "تفسير مميز",
    "creative_copywriting" => "الكتابة الإبداعية",
];
