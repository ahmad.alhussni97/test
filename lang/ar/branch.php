<?php
return [
    'saturday' => 'السبت',
    'sunday' => 'الأحد',
    'monday' => 'الأثنين' ,
    'tuesday' => 'الثلاثاء',
    'wednesday' => 'الأربعاء',
    'thursday' => 'الخميس',
    'friday' => 'الجمعة'
];
