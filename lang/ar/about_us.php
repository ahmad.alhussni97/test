<?php

return [
    "about_us" => "معلومات حولنا",
    "playing_words" => "لعب الكلمات للسيطرة على الملفات والقلوب",
    "our_mission" => "مهمتنا",
    "our_vision" => "رؤيتنا",
    "our_value" => "قيمتنا",
    "our_story" => "قصصنا",
    "be_not_afraid" => "لا تخافوا من العظمة ..",
    "some_are_born_great" => "البعض يولد عظماء ، والبعض يحقق العظمة والبعض الآخر تفرض عليهم العظمة.",
    "who_can_benefit" => "من يمكنه الاستفادة من خدمتنا؟",
    "our_commitment_to_your_future" => "التزامنا بمستقبلك",
    "you_become_a_leaders_clients" => "هل أصبحت من العملاء الرائعين؟",
    "useful_resources" => "موارد مفيدة",
    "call_to_action" => "دعوة للعمل",
    "organizations" => "المنظمات",
    "individuals" => "فردي",
];
