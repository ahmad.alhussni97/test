<?php

return [
    "who_is" => "من هي",
    "leaders_company" => "شركة ليدر",
    "leaders" => "القادة    ",
    "work_values" => "قيمة العمل",
    "responsibility" => "مسؤولية",
    "who_are_our_leaders" => "من هم قادتنا",
    "life_at_leaders" => "الحياة في ليدر",
    "compensation_and_benefits" => "المزايا و التعويض",
    "translation_career" => "مهنة الترجمة",
    "development" => "تطوير"
];

