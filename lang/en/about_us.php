<?php

return [
    "about_us" => "About Us",
    "playing_words" => "playing words to dominate files and hearts",
    "our_mission" => "Our Mission",
    "our_vision" => "Our Vision",
    "our_value" => "Our Value",
    "our_story" => "Our Story",
    "be_not_afraid" => "Be Not Afraid Of Greatness..",
    "some_are_born_great" => "some are born great, some achieve greatness and others have greatness thrust upon them.,",
    "who_can_benefit" => "who can benefit from our service?",
    "our_commitment_to_your_future" => "our commitment to your future",
    "you_become_a_leaders_clients" => "You Become A leaders Clients?",
    "useful_resources" => "Useful Resources",
    "call_to_action" => "Call to action",
    "organizations" => "Organizations",
    "individuals" => "Individuals",
];
