<?php

return [
    "who_is" => "who is",
    "leaders_company" => "leaders company",
    "leaders" => "leaders",
    "work_values" => "work values",
    "responsibility" => "responsibility",
    "who_are_our_leaders" => "who_are_our_leaders",
    "life_at_leaders" => "life at leaders",
    "compensation_and_benefits" => "compensation&benefits",
    "translation_career" => "translation career",
    "development" => "development"
];
