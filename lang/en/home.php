<?php

return [
    "we_speak_your_language" => "we speak your language",
    "here_you_go" => "here you go",
    "what_we_promise_you" => "what we promise you",
    "what_makes_us_different" => "what makes us different",
    "unique_translation" => "Unique translation",
    "distinctive_interpretation" => "Distinctive interpretation",
    "creative_copywriting" => "Creative copywriting",
];
