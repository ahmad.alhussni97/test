<?php

return [
    'not_found' => "Not Found",
    'faq' => "Faq",
    'privacy&policy' => "Privacy & Policy",
    'terms_of_use' => "Terms Of Use",
    'all' => "All"
];
