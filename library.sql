/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100422
 Source Host           : localhost:3306
 Source Schema         : library

 Target Server Type    : MySQL
 Target Server Version : 100422
 File Encoding         : 65001

 Date: 10/02/2023 18:34:02
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for attribute_values
-- ----------------------------
DROP TABLE IF EXISTS `attribute_values`;
CREATE TABLE `attribute_values`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `attribute_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `attribute_values_attribute_id_foreign`(`attribute_id`) USING BTREE,
  CONSTRAINT `attribute_values_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for attributes
-- ----------------------------
DROP TABLE IF EXISTS `attributes`;
CREATE TABLE `attributes`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `attributes_name_unique`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for authors
-- ----------------------------
DROP TABLE IF EXISTS `authors`;
CREATE TABLE `authors`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `educational_degree` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for authors_books
-- ----------------------------
DROP TABLE IF EXISTS `authors_books`;
CREATE TABLE `authors_books`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `author_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `book_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `authors_books_author_id_book_id_unique`(`author_id`, `book_id`) USING BTREE,
  INDEX `authors_books_book_id_foreign`(`book_id`) USING BTREE,
  INDEX `authors_books_author_id_book_id_index`(`author_id`, `book_id`) USING BTREE,
  CONSTRAINT `authors_books_author_id_foreign` FOREIGN KEY (`author_id`) REFERENCES `authors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `authors_books_book_id_foreign` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for book_categories
-- ----------------------------
DROP TABLE IF EXISTS `book_categories`;
CREATE TABLE `book_categories`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `book_categories_parent_id_foreign`(`parent_id`) USING BTREE,
  CONSTRAINT `book_categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `book_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for book_details
-- ----------------------------
DROP TABLE IF EXISTS `book_details`;
CREATE TABLE `book_details`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `book_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `book_details_page_id_book_id_unique`(`page_id`, `book_id`) USING BTREE,
  INDEX `book_details_book_id_foreign`(`book_id`) USING BTREE,
  INDEX `book_details_page_id_book_id_index`(`page_id`, `book_id`) USING BTREE,
  CONSTRAINT `book_details_book_id_foreign` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for books
-- ----------------------------
DROP TABLE IF EXISTS `books`;
CREATE TABLE `books`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `page_count` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `cat_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `books_cat_id_foreign`(`cat_id`) USING BTREE,
  CONSTRAINT `books_cat_id_foreign` FOREIGN KEY (`cat_id`) REFERENCES `book_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `categories_name_unique`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp(0) NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `failed_jobs_uuid_unique`(`uuid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for media
-- ----------------------------
DROP TABLE IF EXISTS `media`;
CREATE TABLE `media`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `model_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  `uuid` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `collection_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `disk` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `conversions_disk` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `size` bigint(20) UNSIGNED NOT NULL,
  `manipulations` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `custom_properties` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `generated_conversions` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `responsive_images` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `order_column` int(10) UNSIGNED NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `media_uuid_unique`(`uuid`) USING BTREE,
  INDEX `media_model_type_model_id_index`(`model_type`, `model_id`) USING BTREE,
  INDEX `media_order_column_index`(`order_column`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (4, '2019_12_14_000001_create_personal_access_tokens_table', 1);
INSERT INTO `migrations` VALUES (5, '2022_11_21_094915_create_permission_tables', 1);
INSERT INTO `migrations` VALUES (6, '2022_12_08_083824_create_media_table', 1);
INSERT INTO `migrations` VALUES (7, '2023_01_10_064613_create_categories_table', 1);
INSERT INTO `migrations` VALUES (8, '2023_01_10_065344_create_products_table', 1);
INSERT INTO `migrations` VALUES (9, '2023_01_10_163503_create_attributes_table', 1);
INSERT INTO `migrations` VALUES (10, '2023_01_10_163645_create_attribute_values', 1);
INSERT INTO `migrations` VALUES (11, '2023_01_10_200137_create_product_attribute_table', 1);
INSERT INTO `migrations` VALUES (12, '2023_02_10_125247_create_books_table', 1);
INSERT INTO `migrations` VALUES (13, '2023_02_10_125300_create_authors_table', 1);
INSERT INTO `migrations` VALUES (14, '2023_02_10_125401_create_authors_books_table', 1);
INSERT INTO `migrations` VALUES (15, '2023_02_10_140130_craete_book_details_table', 1);
INSERT INTO `migrations` VALUES (16, '2023_02_10_140931_book_categories', 1);
INSERT INTO `migrations` VALUES (17, '2023_02_10_141657_add_column_to_books_table', 1);

-- ----------------------------
-- Table structure for model_has_permissions
-- ----------------------------
DROP TABLE IF EXISTS `model_has_permissions`;
CREATE TABLE `model_has_permissions`  (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`, `model_id`, `model_type`) USING BTREE,
  INDEX `model_has_permissions_model_id_model_type_index`(`model_id`, `model_type`) USING BTREE,
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for model_has_roles
-- ----------------------------
DROP TABLE IF EXISTS `model_has_roles`;
CREATE TABLE `model_has_roles`  (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`role_id`, `model_id`, `model_type`) USING BTREE,
  INDEX `model_has_roles_model_id_model_type_index`(`model_id`, `model_type`) USING BTREE,
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of model_has_roles
-- ----------------------------
INSERT INTO `model_has_roles` VALUES (4, 'App\\Models\\User', 4);
INSERT INTO `model_has_roles` VALUES (5, 'App\\Models\\User', 5);
INSERT INTO `model_has_roles` VALUES (6, 'App\\Models\\User', 6);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `permissions_name_guard_name_unique`(`name`, `guard_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 159 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES (80, 'access_dashboard', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (81, 'access_profile', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (82, 'show-role', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (83, 'create-role', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (84, 'update-role', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (85, 'delete-role', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (86, 'show-user', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (87, 'create-user', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (88, 'update-user', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (89, 'delete-user', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (90, 'show-menu', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (91, 'create-menu', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (92, 'update-menu', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (93, 'delete-menu', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (94, 'show-service_main_page', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (95, 'update-service_main_page', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (96, 'show-service', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (97, 'create-service', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (98, 'update-service', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (99, 'delete-service', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (100, 'show-language', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (101, 'create-language', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (102, 'update-language', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (103, 'delete-language', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (104, 'show-service-form', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (105, 'show-about-us', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (106, 'update-about-us', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (107, 'show-faq', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (108, 'create-faq', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (109, 'update-faq', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (110, 'delete-faq', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (111, 'show-privacy-and-policy', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (112, 'create-privacy-and-policy', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (113, 'update-privacy-and-policy', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (114, 'delete-privacy-and-policy', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (115, 'show-term-of-use', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (116, 'create-term-of-use', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (117, 'update-term-of-use', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (118, 'delete-term-of-use', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (119, 'show-client', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (120, 'create-client', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (121, 'update-client', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (122, 'delete-client', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (123, 'show-client-category', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (124, 'create-client-category', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (125, 'update-client-category', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (126, 'delete-client-category', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (127, 'show-blog', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (128, 'create-blog', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (129, 'update-blog', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (130, 'delete-blog', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (131, 'show-blog-category', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (132, 'create-blog-category', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (133, 'update-blog-category', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (134, 'delete-blog-category', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (135, 'show-contact-us-info', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (136, 'update-contact-us-info', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (137, 'show-contact-us-form', 'web', '2023-02-10 14:19:35', '2023-02-10 14:19:35');
INSERT INTO `permissions` VALUES (138, 'show-branch', 'web', '2023-02-10 14:19:36', '2023-02-10 14:19:36');
INSERT INTO `permissions` VALUES (139, 'update-branch', 'web', '2023-02-10 14:19:36', '2023-02-10 14:19:36');
INSERT INTO `permissions` VALUES (140, 'show-headline', 'web', '2023-02-10 14:19:36', '2023-02-10 14:19:36');
INSERT INTO `permissions` VALUES (141, 'update-headline', 'web', '2023-02-10 14:19:36', '2023-02-10 14:19:36');
INSERT INTO `permissions` VALUES (142, 'show-career_main_page', 'web', '2023-02-10 14:19:36', '2023-02-10 14:19:36');
INSERT INTO `permissions` VALUES (143, 'update-career_main_page', 'web', '2023-02-10 14:19:36', '2023-02-10 14:19:36');
INSERT INTO `permissions` VALUES (144, 'show-career', 'web', '2023-02-10 14:19:36', '2023-02-10 14:19:36');
INSERT INTO `permissions` VALUES (145, 'create-career', 'web', '2023-02-10 14:19:36', '2023-02-10 14:19:36');
INSERT INTO `permissions` VALUES (146, 'update-career', 'web', '2023-02-10 14:19:36', '2023-02-10 14:19:36');
INSERT INTO `permissions` VALUES (147, 'delete-career', 'web', '2023-02-10 14:19:36', '2023-02-10 14:19:36');
INSERT INTO `permissions` VALUES (148, 'show-department', 'web', '2023-02-10 14:19:36', '2023-02-10 14:19:36');
INSERT INTO `permissions` VALUES (149, 'update-department', 'web', '2023-02-10 14:19:36', '2023-02-10 14:19:36');
INSERT INTO `permissions` VALUES (150, 'show-career-form', 'web', '2023-02-10 14:19:36', '2023-02-10 14:19:36');
INSERT INTO `permissions` VALUES (151, 'download-career-cv', 'web', '2023-02-10 14:19:36', '2023-02-10 14:19:36');
INSERT INTO `permissions` VALUES (152, 'show-home_page', 'web', '2023-02-10 14:19:36', '2023-02-10 14:19:36');
INSERT INTO `permissions` VALUES (153, 'update-home_page', 'web', '2023-02-10 14:19:36', '2023-02-10 14:19:36');
INSERT INTO `permissions` VALUES (154, 'show-setting', 'web', '2023-02-10 14:19:36', '2023-02-10 14:19:36');
INSERT INTO `permissions` VALUES (155, 'update-setting', 'web', '2023-02-10 14:19:36', '2023-02-10 14:19:36');
INSERT INTO `permissions` VALUES (156, 'show-get_a_quote_page', 'web', '2023-02-10 14:19:36', '2023-02-10 14:19:36');
INSERT INTO `permissions` VALUES (157, 'update-get_a_quote_page', 'web', '2023-02-10 14:19:36', '2023-02-10 14:19:36');
INSERT INTO `permissions` VALUES (158, 'show-get_a_quote-form', 'web', '2023-02-10 14:19:36', '2023-02-10 14:19:36');

-- ----------------------------
-- Table structure for personal_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `last_used_at` timestamp(0) NULL DEFAULT NULL,
  `expires_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `personal_access_tokens_token_unique`(`token`) USING BTREE,
  INDEX `personal_access_tokens_tokenable_type_tokenable_id_index`(`tokenable_type`, `tokenable_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for product_attributes
-- ----------------------------
DROP TABLE IF EXISTS `product_attributes`;
CREATE TABLE `product_attributes`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `value_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `product_attributes_value_id_product_id_unique`(`value_id`, `product_id`) USING BTREE,
  INDEX `product_attributes_product_id_foreign`(`product_id`) USING BTREE,
  INDEX `product_attributes_value_id_product_id_index`(`value_id`, `product_id`) USING BTREE,
  CONSTRAINT `product_attributes_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `product_attributes_value_id_foreign` FOREIGN KEY (`value_id`) REFERENCES `attribute_values` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `category_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `products_name_unique`(`name`) USING BTREE,
  INDEX `products_category_id_foreign`(`category_id`) USING BTREE,
  CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for role_has_permissions
-- ----------------------------
DROP TABLE IF EXISTS `role_has_permissions`;
CREATE TABLE `role_has_permissions`  (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`, `role_id`) USING BTREE,
  INDEX `role_has_permissions_role_id_foreign`(`role_id`) USING BTREE,
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role_has_permissions
-- ----------------------------
INSERT INTO `role_has_permissions` VALUES (80, 4);
INSERT INTO `role_has_permissions` VALUES (80, 5);
INSERT INTO `role_has_permissions` VALUES (81, 4);
INSERT INTO `role_has_permissions` VALUES (81, 5);
INSERT INTO `role_has_permissions` VALUES (82, 4);
INSERT INTO `role_has_permissions` VALUES (82, 5);
INSERT INTO `role_has_permissions` VALUES (83, 4);
INSERT INTO `role_has_permissions` VALUES (83, 5);
INSERT INTO `role_has_permissions` VALUES (84, 4);
INSERT INTO `role_has_permissions` VALUES (84, 5);
INSERT INTO `role_has_permissions` VALUES (85, 4);
INSERT INTO `role_has_permissions` VALUES (85, 5);
INSERT INTO `role_has_permissions` VALUES (86, 4);
INSERT INTO `role_has_permissions` VALUES (86, 5);
INSERT INTO `role_has_permissions` VALUES (87, 4);
INSERT INTO `role_has_permissions` VALUES (87, 5);
INSERT INTO `role_has_permissions` VALUES (88, 4);
INSERT INTO `role_has_permissions` VALUES (88, 5);
INSERT INTO `role_has_permissions` VALUES (89, 4);
INSERT INTO `role_has_permissions` VALUES (89, 5);
INSERT INTO `role_has_permissions` VALUES (90, 4);
INSERT INTO `role_has_permissions` VALUES (90, 5);
INSERT INTO `role_has_permissions` VALUES (91, 4);
INSERT INTO `role_has_permissions` VALUES (91, 5);
INSERT INTO `role_has_permissions` VALUES (92, 4);
INSERT INTO `role_has_permissions` VALUES (92, 5);
INSERT INTO `role_has_permissions` VALUES (93, 4);
INSERT INTO `role_has_permissions` VALUES (93, 5);
INSERT INTO `role_has_permissions` VALUES (94, 4);
INSERT INTO `role_has_permissions` VALUES (94, 5);
INSERT INTO `role_has_permissions` VALUES (95, 4);
INSERT INTO `role_has_permissions` VALUES (95, 5);
INSERT INTO `role_has_permissions` VALUES (96, 4);
INSERT INTO `role_has_permissions` VALUES (96, 5);
INSERT INTO `role_has_permissions` VALUES (97, 4);
INSERT INTO `role_has_permissions` VALUES (97, 5);
INSERT INTO `role_has_permissions` VALUES (98, 4);
INSERT INTO `role_has_permissions` VALUES (98, 5);
INSERT INTO `role_has_permissions` VALUES (99, 4);
INSERT INTO `role_has_permissions` VALUES (99, 5);
INSERT INTO `role_has_permissions` VALUES (100, 4);
INSERT INTO `role_has_permissions` VALUES (100, 5);
INSERT INTO `role_has_permissions` VALUES (101, 4);
INSERT INTO `role_has_permissions` VALUES (101, 5);
INSERT INTO `role_has_permissions` VALUES (102, 4);
INSERT INTO `role_has_permissions` VALUES (102, 5);
INSERT INTO `role_has_permissions` VALUES (103, 4);
INSERT INTO `role_has_permissions` VALUES (103, 5);
INSERT INTO `role_has_permissions` VALUES (104, 4);
INSERT INTO `role_has_permissions` VALUES (104, 5);
INSERT INTO `role_has_permissions` VALUES (105, 4);
INSERT INTO `role_has_permissions` VALUES (105, 5);
INSERT INTO `role_has_permissions` VALUES (106, 4);
INSERT INTO `role_has_permissions` VALUES (106, 5);
INSERT INTO `role_has_permissions` VALUES (107, 4);
INSERT INTO `role_has_permissions` VALUES (107, 5);
INSERT INTO `role_has_permissions` VALUES (108, 4);
INSERT INTO `role_has_permissions` VALUES (108, 5);
INSERT INTO `role_has_permissions` VALUES (109, 4);
INSERT INTO `role_has_permissions` VALUES (109, 5);
INSERT INTO `role_has_permissions` VALUES (110, 4);
INSERT INTO `role_has_permissions` VALUES (110, 5);
INSERT INTO `role_has_permissions` VALUES (111, 4);
INSERT INTO `role_has_permissions` VALUES (111, 5);
INSERT INTO `role_has_permissions` VALUES (112, 4);
INSERT INTO `role_has_permissions` VALUES (112, 5);
INSERT INTO `role_has_permissions` VALUES (113, 4);
INSERT INTO `role_has_permissions` VALUES (113, 5);
INSERT INTO `role_has_permissions` VALUES (114, 4);
INSERT INTO `role_has_permissions` VALUES (114, 5);
INSERT INTO `role_has_permissions` VALUES (115, 4);
INSERT INTO `role_has_permissions` VALUES (115, 5);
INSERT INTO `role_has_permissions` VALUES (116, 4);
INSERT INTO `role_has_permissions` VALUES (116, 5);
INSERT INTO `role_has_permissions` VALUES (117, 4);
INSERT INTO `role_has_permissions` VALUES (117, 5);
INSERT INTO `role_has_permissions` VALUES (118, 4);
INSERT INTO `role_has_permissions` VALUES (118, 5);
INSERT INTO `role_has_permissions` VALUES (119, 4);
INSERT INTO `role_has_permissions` VALUES (119, 5);
INSERT INTO `role_has_permissions` VALUES (120, 4);
INSERT INTO `role_has_permissions` VALUES (120, 5);
INSERT INTO `role_has_permissions` VALUES (121, 4);
INSERT INTO `role_has_permissions` VALUES (121, 5);
INSERT INTO `role_has_permissions` VALUES (122, 4);
INSERT INTO `role_has_permissions` VALUES (122, 5);
INSERT INTO `role_has_permissions` VALUES (123, 4);
INSERT INTO `role_has_permissions` VALUES (123, 5);
INSERT INTO `role_has_permissions` VALUES (124, 4);
INSERT INTO `role_has_permissions` VALUES (124, 5);
INSERT INTO `role_has_permissions` VALUES (125, 4);
INSERT INTO `role_has_permissions` VALUES (125, 5);
INSERT INTO `role_has_permissions` VALUES (126, 4);
INSERT INTO `role_has_permissions` VALUES (126, 5);
INSERT INTO `role_has_permissions` VALUES (127, 4);
INSERT INTO `role_has_permissions` VALUES (127, 5);
INSERT INTO `role_has_permissions` VALUES (128, 4);
INSERT INTO `role_has_permissions` VALUES (128, 5);
INSERT INTO `role_has_permissions` VALUES (129, 4);
INSERT INTO `role_has_permissions` VALUES (129, 5);
INSERT INTO `role_has_permissions` VALUES (130, 4);
INSERT INTO `role_has_permissions` VALUES (130, 5);
INSERT INTO `role_has_permissions` VALUES (131, 4);
INSERT INTO `role_has_permissions` VALUES (131, 5);
INSERT INTO `role_has_permissions` VALUES (132, 4);
INSERT INTO `role_has_permissions` VALUES (132, 5);
INSERT INTO `role_has_permissions` VALUES (133, 4);
INSERT INTO `role_has_permissions` VALUES (133, 5);
INSERT INTO `role_has_permissions` VALUES (134, 4);
INSERT INTO `role_has_permissions` VALUES (134, 5);
INSERT INTO `role_has_permissions` VALUES (135, 4);
INSERT INTO `role_has_permissions` VALUES (135, 5);
INSERT INTO `role_has_permissions` VALUES (136, 4);
INSERT INTO `role_has_permissions` VALUES (136, 5);
INSERT INTO `role_has_permissions` VALUES (137, 4);
INSERT INTO `role_has_permissions` VALUES (137, 5);
INSERT INTO `role_has_permissions` VALUES (138, 4);
INSERT INTO `role_has_permissions` VALUES (138, 5);
INSERT INTO `role_has_permissions` VALUES (139, 4);
INSERT INTO `role_has_permissions` VALUES (139, 5);
INSERT INTO `role_has_permissions` VALUES (140, 4);
INSERT INTO `role_has_permissions` VALUES (140, 5);
INSERT INTO `role_has_permissions` VALUES (141, 4);
INSERT INTO `role_has_permissions` VALUES (141, 5);
INSERT INTO `role_has_permissions` VALUES (142, 4);
INSERT INTO `role_has_permissions` VALUES (142, 5);
INSERT INTO `role_has_permissions` VALUES (143, 4);
INSERT INTO `role_has_permissions` VALUES (143, 5);
INSERT INTO `role_has_permissions` VALUES (144, 4);
INSERT INTO `role_has_permissions` VALUES (144, 5);
INSERT INTO `role_has_permissions` VALUES (145, 4);
INSERT INTO `role_has_permissions` VALUES (145, 5);
INSERT INTO `role_has_permissions` VALUES (146, 4);
INSERT INTO `role_has_permissions` VALUES (146, 5);
INSERT INTO `role_has_permissions` VALUES (147, 4);
INSERT INTO `role_has_permissions` VALUES (147, 5);
INSERT INTO `role_has_permissions` VALUES (148, 4);
INSERT INTO `role_has_permissions` VALUES (148, 5);
INSERT INTO `role_has_permissions` VALUES (149, 4);
INSERT INTO `role_has_permissions` VALUES (149, 5);
INSERT INTO `role_has_permissions` VALUES (150, 4);
INSERT INTO `role_has_permissions` VALUES (150, 5);
INSERT INTO `role_has_permissions` VALUES (151, 4);
INSERT INTO `role_has_permissions` VALUES (151, 5);
INSERT INTO `role_has_permissions` VALUES (152, 4);
INSERT INTO `role_has_permissions` VALUES (152, 5);
INSERT INTO `role_has_permissions` VALUES (153, 4);
INSERT INTO `role_has_permissions` VALUES (153, 5);
INSERT INTO `role_has_permissions` VALUES (154, 4);
INSERT INTO `role_has_permissions` VALUES (154, 5);
INSERT INTO `role_has_permissions` VALUES (155, 4);
INSERT INTO `role_has_permissions` VALUES (155, 5);
INSERT INTO `role_has_permissions` VALUES (156, 4);
INSERT INTO `role_has_permissions` VALUES (156, 5);
INSERT INTO `role_has_permissions` VALUES (157, 4);
INSERT INTO `role_has_permissions` VALUES (157, 5);
INSERT INTO `role_has_permissions` VALUES (158, 4);
INSERT INTO `role_has_permissions` VALUES (158, 5);

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `roles_name_guard_name_unique`(`name`, `guard_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES (4, 'super_admin', 'web', '2023-02-10 14:19:36', '2023-02-10 14:19:36');
INSERT INTO `roles` VALUES (5, 'admin', 'web', '2023-02-10 14:19:36', '2023-02-10 14:19:36');
INSERT INTO `roles` VALUES (6, 'user', 'web', '2023-02-10 14:19:36', '2023-02-10 14:19:36');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (4, 'super_admin', 'super_admin@gmail.com', NULL, '$2y$10$PUFnY7LrtyhIzn9zzW5zM.bxzsr5oZmZLxcxAC/tXTnCr5povH5N.', NULL, '2023-02-10 14:19:36', '2023-02-10 14:19:36');
INSERT INTO `users` VALUES (5, 'admin', 'admin@gmail.com', NULL, '$2y$10$RMMMiKY/bPOskwkrTcQdae7b9t4MSbVXXV8iUDSgpQaRtZs0fjAAO', NULL, '2023-02-10 14:19:36', '2023-02-10 14:19:36');
INSERT INTO `users` VALUES (6, 'user', 'user@gmail.com', NULL, '$2y$10$hN/FyYoY150EZDkqMZSIf.aMkAzNvMOOi4RVAgtQwpQD5EiS9d3Zi', NULL, '2023-02-10 14:19:36', '2023-02-10 14:19:36');

SET FOREIGN_KEY_CHECKS = 1;
