<?php

use App\Models\AttributeValue;
use App\Models\ProductAttribute;

function checkVariable($variable, $val = null)
{
    if (isset($variable) && !empty($variable)) {
        if (is_null($val)) return true;
        else if ($variable == $val) return true;
    }

    return false;
}

function getMenusDashboard()
{

    return [
        [
            "name" => ucwords("Dashboard"),
            "icon" => "fa fa-home",
            "url" => route("dashboard"),
        ],
        [
            "name" => ucwords("Books"),
            "icon" => "fa fa-book",
            "url" => "",
            "sub_menu" => [
                [
                    "name" => ucwords("all Books"),
                    "url" => route("book.index"),
                ],
                [
                    "name" => ucwords("add Book"),
                    "url" => route("book.create"),
                ]
            ]
        ],
        [
            "name" => ucwords("Authors"),
            "icon" => "fa fa-user",
            "url" => "",
            "sub_menu" => [
                [
                    "name" => ucwords("all Authors"),
                    "url" => route("author.index"),
                ],
                [
                    "name" => ucwords("add Author"),
                    "url" => route("author.create"),
                ]
            ]
        ],
        [
            "name" => ucwords("StackOverFlow"),
            "icon" => "fa fa-question",
            "url" => "",
            "sub_menu" => [
                [
                    "name" => ucwords("all Question"),
                    "url" => route("stack_over_flow.index"),
                ]
            ]
        ],
        [
            "name" => ucwords("Vanilla js"),
            "icon" => "fa fa-question",
            "url" => "",
            "sub_menu" => [
                [
                    "name" => ucwords("all Question"),
                    "url" => route("vanilla_js.index"),
                ]
            ]
        ],
    ];
}

function getUserInfo()
{
    return
        (object)[
            "id" => Auth()->user()->id ?? '',
            "img" => asset("assets/dashboard/images/layout/user.png"),
            "name" => Auth()->user()->name ?? '',
            "role" => Auth()->user()->roles->first()->name ?? 'user'
        ];
}

function getWebsiteInfo()
{
    return (object)[
        'logo' => asset('assets/dashboard/images/layout/logo.svg'),
        'logoMini' => asset('assets/dashboard/images/layout/logo-mini.svg')
    ];
}


function getEditButton($route, $id)
{
    $btn = '<a  href="' . route($route, ["id" => $id]) . '" class="edit-icon mr-2"><span><i class="mdi mdi-tag"></i></span></a>';

    return $btn;
}


function getDeleteButton($route, $id)
{
    $btn = '<span class="btn btn-sm btn-clean btn-icon width-auto delete-item delete-icon" data-url="' . route($route, ["id" => $id]) . '"><i class="mdi mdi-delete mdi-delete-custom"></i></span>';

    return $btn;
}

function getShowButton($route, $id)
{
    $btn = '<a  href="' . route($route, ["id" => $id]) . '" class="show-icon mr-2"><span><i class="mdi mdi-eye"></i></span></a>';

    return $btn;
}

function getLanguages()
{
    return app("config")["translatable"]["locales"];
}

function getDataStore($data)
{
    $arr = [];
    $lang = app()->getLocale();

    $arr[$lang] = $data;

    return $arr[$lang];
}

function getCurrentLang()
{
    return app()->getLocale();
}

function getShowPage($showPage)
{
    $arr = [];
    $menu = Menu::all();

    foreach ($menu as $m) {

        $values = ["value" => 0, "id" => -1];
        $values["id"] = $m->id;

        if (!is_null($showPage))
            $values["value"] = in_array($m->id, $showPage) ? 1 : 0;

        array_push($arr, $values);
    }

    return json_encode($arr);
}

function checkIfImageExist($img): array
{
    if (isset($img) && checkVariable($img)) {
        $img = strstr($img, '/storage');

        if (file_exists(public_path($img))) {
            $img = Request::root() . $img;
            return ["url" => $img, "result" => true];
        }
    }

    return ["url" => '', "result" => false];
}

function getAttributeValues($attr_id){

    $data=AttributeValue::where("attribute_id",$attr_id)->get();

    return $data;
}

function checkProductAttribute($product_id,$value_id){

    $data=ProductAttribute::where("product_id",$product_id)->where("value_id",$value_id)->first();

    if(isset($data) && checkVariable($data))
        return  true;

    return false;
}
