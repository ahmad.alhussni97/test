<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckPermission
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param Closure $next
     * @param $permission
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */

    public function handle(Request $request, Closure $next, $permission)
    {
        // if user is visitor ||  does not have the required permission
        if (auth()->user() == null || !auth()->user()->can($permission)) {
            if (auth()->user()) {
                // Check Permission access_dashboard
                if (!auth()->user()->can($permission) && $permission == "access_dashboard") {
                    Auth::logout();
                    $request->session()->invalidate();
                    return redirect(route('login'))->withErrors(['permission' => 'You don\'t  have permission to access dashboard']);
                } elseif (!auth()->user()->can($permission)) {
                    return response('You don\'t  have permission '.$permission, 403);
                }
            }
        }

        return $next($request);
    }
}
