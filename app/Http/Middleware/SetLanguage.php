<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class SetLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {

        $lang='';

        // found request lang
        if ($request->has("lang") && in_array($request->input("lang"), getLanguages())
            && $request->input("lang") != session()->get("locale")) {

            $lang = $request->input("lang");
            session()->put('locale', $lang);


        } else { // found  lang  storage previously
            $lang = "en";// default language
            if (Session::has('locale'))
                $lang = Session::get('locale');
        }

        // set language
        App::setLocale($lang);

        return $next($request);
    }
}
