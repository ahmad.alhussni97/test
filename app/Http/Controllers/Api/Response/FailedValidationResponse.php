<?php


namespace App\Http\Controllers\Api\Response;


use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

trait FailedValidationResponse
{
    protected function failedValidation(Validator $validator)
    {
        $response = new ResponseBuilder();
        $errors = [];
        foreach ($validator->errors()->messages() as $attribute => $message) {
            $errors[$attribute] = array_value_first($message);
        }
        $response = $response->withMessage('The given data is invalid')
            ->withResponse('INVALID_DATA')
            ->withHttpCode(422)
            ->withSuccessFlag(false)
            ->withErrors($errors)
            ->build();
        throw new ValidationException($validator, $response);
    }
}
