<?php


namespace App\Http\Controllers\Api\Response;


use Illuminate\Http\JsonResponse;

class ResponseBuilder
{
    /**
     * @var int
     */
    private $httpCode = 200;

    /**
     * @var string
     */
    private $message = 'SUCCESS';

    /**
     * @var array
     */
    private $data;

    /**
     * @var string
     */
    private $response = 'SUCCESS';

    /**
     * @var bool
     */
    private $success = true;

    /**
     * @var array
     */
    private $errors;

    public function __construct()
    {
        // Create an empty object instead of the empty array
        $this->data = new \stdClass();
        $this->errors = new \stdClass();
    }

    public function withData(array $data = []): ResponseBuilder
    {
        $this->data = $data;
        return $this;
    }

    public function withHttpCode(int $code = 200): ResponseBuilder
    {
        $this->httpCode = $code;
        return $this;
    }

    public function withMessage(string $message = 'SUCCESS'): ResponseBuilder
    {
        $this->message = $message;
        return $this;
    }

    public function withResponse(string $response = 'SUCCESS'): ResponseBuilder
    {
        $this->response = $response;
        return $this;
    }

    public function withSuccessFlag(bool $success = true): ResponseBuilder
    {
        $this->success = $success;
        return $this;
    }

    public function withErrors(array $errors = []): ResponseBuilder
    {
        $this->errors = $errors;
        return $this;
    }

    public function build(): JsonResponse
    {
        return response()->json([
            'success' => $this->success,
            'data' => $this->data,
            'errors' => $this->errors,
            'message' => $this->message,
            'response' => $this->response
        ], $this->httpCode);
    }
}
