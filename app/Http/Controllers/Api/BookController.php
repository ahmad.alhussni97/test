<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Response\ResponseBuilder;
use App\Http\Controllers\Controller;
use App\Http\Resources\AuthorResource;
use App\Http\Resources\BookResource;
use App\Http\Resources\CategoryResource;
use App\Models\Author;
use App\Models\Book;
use App\Models\BookCategory;
use App\Models\BookDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class BookController extends Controller
{
    private $response;

    public function __construct(ResponseBuilder $response)
    {
        $this->response = $response;
    }

    public function index()
    {
        $books = Book::paginate(10);
        return $this->response->withData(BookResource::collection($books)->toArray(\request()))
            ->withResponse('SUCCESS')
            ->withMessage('Data Retrieved Successfully')
            ->build();
    }

    public function getBook($id)
    {
        $books = Book::where('id', $id)->get();
        return $this->response->withData(BookResource::collection($books)->toArray(\request()))
            ->withResponse('SUCCESS')
            ->withMessage('Data Retrieved Successfully')
            ->build();
    }

    public function getCategories()
    {
        $categories = BookCategory::all()->pluck('name');
        return $this->response->withData($categories->toArray())
            ->withResponse('SUCCESS')
            ->withMessage('Data Retrieved Successfully')
            ->build();
    }

    public function getCategoriesTree()
    {
        $categories = BookCategory::whereNull('parent_id')->get();
        return $this->response->withData(CategoryResource::collection($categories)->toArray(\request()))
            ->withResponse('SUCCESS')
            ->withMessage('Data Retrieved Successfully')
            ->build();
    }

    public function getAuthors()
    {
        $authors = Author::all();
        return $this->response->withData(AuthorResource::collection($authors)->toArray(\request()))
            ->withResponse('SUCCESS')
            ->withMessage('Data Retrieved Successfully')
            ->build();
    }

}
