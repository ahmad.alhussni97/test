<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public $function;
    public $class;
    public $pageTitle;

    public function __construct()
    {
        $this->class = 'home';
        $this->pageTitle = "Library System";
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $this->function = __FUNCTION__;

        return view("dashboard.main")->with(["class" => $this->class, "function" => $this->function, "pageTitle" => $this->pageTitle]);
    }

}
