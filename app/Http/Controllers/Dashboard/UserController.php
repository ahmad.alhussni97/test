<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Membership;
use App\Models\User;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class UserController extends Controller
{

    public $class;
    public $function;
    public $pageTitle;

    public function __construct()
    {
        $this->class = "user";
    }

    public function dataTable(Request $request)
    {
        if ($request->ajax()) {

            $data = User::orderBy('created_at', 'DESC');

            return Datatables::eloquent($data)
                ->addColumn('action', function ($row) {
                    $btn = '';

                    if (Auth::user()->can("update-user"))
                        $btn .= getEditButton("user.edit",$row->id);


                    if (Auth::user()->can("show-user"))
                        $btn .= getShowButton("user.show",$row->id);


                    if (Auth::user()->can('delete-user'))
                        $btn .= getDeleteButton("user.destroy",$row->id);


                    return $btn;

                })
                ->addIndexColumn()
                ->make(true);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->function = __FUNCTION__;
        $this->pageTitle = "All User";

        return view("dashboard.main")->with(["pageTitle" => $this->pageTitle, "class" => $this->class, "function" => $this->function]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->function = __FUNCTION__;
        $this->pageTitle = "Create New User";
        $roles = User::getAllRoles();
        $data = ["roles" => $roles];

        return view("dashboard.main", compact("data"))->with(["pageTitle" => $this->pageTitle, "class" => $this->class, "function" => $this->function]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
            'name' => 'required',
            'email' => array('required', 'regex:/(.+)@(.+)\.(.+)/i', 'unique:users,email'),
            'password' => 'required|min:8',
        ], [
            'email.regex' => 'The email format is not valid',
        ]);

        if ($validator) {
            DB::beginTransaction();
            $attr = $request->only(User::getFillables());
            $attr['password'] = bcrypt($request['password']);
            $user = new User($attr);
            $user->save();

            if ($request['roles']) {
                $user->assignRole($request['roles']);
            } else {
                //default role for browse all website
                $user->assignRole('user');
            }
            DB::commit();

            return redirect(route('user.edit', $user))->with('result','User Added successfully');
        } else {
            return abort(500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $allRoles = ' ';
        $this->function = __FUNCTION__;
        $user = User::with('roles')->where("id", $id)->firstOrFail();
        $this->pageTitle = "Show User " . Str::ucfirst($user->name) ?? '';

        foreach ($user->roles as $key=>$role) $allRoles .="Role : ". Str::ucfirst($role->name) ."  -  ";

        $data = ["user" => $user, 'roles' => $allRoles];

        return view("dashboard.main", compact("data"))->with(["pageTitle" => $this->pageTitle, "class" => $this->class, "function" => $this->function]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->function = __FUNCTION__;
        $user = User::with('roles')->where("id", $id)->firstOrFail();
        $this->pageTitle = "Edit User " . Str::ucfirst($user->name) ?? '';
        $roles = User::getAllRoles();
        $data = ["id" => $id, "user" => $user, "roles" => $roles];

        return view("dashboard.main", compact("data"))->with(["pageTitle" => $this->pageTitle, "class" => $this->class, "function" => $this->function]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $request->validate([
            'name' => 'required',
            'email' => array('required', 'regex:/(.+)@(.+)\.(.+)/i', 'unique:users,email,' . $id),
            'password' => 'nullable|min:8',
        ], [
            'email.regex' => 'The email format is not valid',
        ]);

        if ($validator) {
            DB::beginTransaction();
            $user = User::find($id);
            $attr = $request->only(User::getFillables());
            $attr['password'] = $attr["password"] ? bcrypt($request['password']) : $user->password;
            $user->update($attr);

            if ($request['roles']) {
                $user->syncRoles($request['roles']);
            } else {
                //default role for browse all website
                $user->syncRoles(['user']);
            }
            DB::commit();

            return redirect(route('user.edit', $user))->with('result', 'User Updated successfully');
        } else {
            return abort(500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        $data['code'] = 1;
        $data['msg'] = 'User Deleted successfully';

        return response()->json($data);

    }
}
