<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;

class StackOverFlowController extends Controller
{
    public $function;
    public $class;
    public $pageTitle;
    public $api;

    public function __construct()
    {
        $this->class = 'stack_over_flow';
        $this->api = 'https://my.api.mockaroo.com/question_api';
    }


    public function index()
    {
        $jsonData = Http::get($this->api, [
            'key' => '95090cb0'
        ])->json();

        $this->pageTitle = "All Question";
        $this->function = __FUNCTION__;
        $jsonData = array_reverse($jsonData);
        $jsonData = array_slice($jsonData, 50);
        Session::forget('question');
        Session::put('question', $jsonData);
        Session::save();
        $data = ["response" => $jsonData];
        return view("dashboard.main", compact("data"))->with(["pageTitle" => $this->pageTitle, "class" => $this->class, "function" => $this->function]);
    }

    public function showData($id)
    {
        if ($data = Session::get('question')) {

            $this->function = "show";
            $this->pageTitle = "Question Details";

            $data = array_filter($data, function ($record) use ($id) {
                return ($record["id"] == $id) ? $record : null;
            });

            if (!is_null($data)) {
                $data = $data[array_key_first($data)];
                return view("dashboard.main", compact("data"))->with(["pageTitle" => $this->pageTitle, "class" => $this->class, "function" => $this->function]);
            }
        }

        abort(404);
    }


}
