<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Author;
use App\Models\Book;
use App\Models\BookCategory;
use App\Models\BookDetail;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class BookController extends Controller
{
    public $class;
    public $function;
    public $pageTitle;
    public $searchValue;

    /**
     * BookController constructor.
     */
    public function __construct()
    {
        $this->class = 'book';
        $this->searchValue = '';
    }


    public function dataTable(Request $request)
    {
        if ($request->ajax()) {

            $data = Book::with("bookCategory");

            // get sub category
            if ($request->has("sub_category") && $request->input("sub_category") != null) {
                $subCategory = $request->input("sub_category");
                $data = $data->where("cat_id", $subCategory);

                // get main category
            } elseif ($request->has("main_category") && $request->input("main_category") != null) {
                $mainCategory = $request->input("main_category");
                $ids = [$mainCategory];
                $parentMainCategory = BookCategory::where("parent_id", $mainCategory)->get();

                if ($parentMainCategory)
                    foreach ($parentMainCategory as $parent)
                        array_push($ids, $parent->id);

                $data = $data->whereIn("cat_id", $ids);
            }

            $data = $data->orderBy("id", "ASC");

            return DataTables::eloquent($data)
                ->filterColumn('summary', function ($query, $keyword) {
                    if (!empty($keyword)) {
                        $data = $query->where('summary', 'LIKE', '%' . (string)$keyword . '%')->first();
                        if (isset($data) && checkVariable($data)) return true;
                    }
                    return false;
                })
                ->filterColumn('category', function ($query, $keyword) {
                    if (!empty($keyword)) {
                        $this->searchValue = $keyword;
                        $data = $query->whereHas('bookCategory', function ($q) {
                            $q->where('name', 'LIKE', '%' . (string)$this->searchValue . '%');
                        })->first();
                        if (isset($data) && checkVariable($data)) return true;
                    }
                    return false;
                })
                ->editColumn('summary', function ($row) {
                    return strip_tags($row->summary);
                })
                ->addColumn('category', function ($row) {
                    return $row->bookCategory->name ?? '';
                })
                ->addColumn('action', function ($row) {
                    $btn = '';
                    if (isset($row->bookDetails[0]->id))
                        $btn .= '<a  href="' . route("book_details.show", ["book_id" => $row->id]) . '" class="edit-icon mr-2"><span><i class="mdi mdi-eye"></i></span></a>';
                    else
                        $btn .= '<a  href="' . route("book_details.create", ["book_id" => $row->id]) . '" class="edit-icon mr-2"><span><i class="fa fa-plus"></i></span></a>';

                    return $btn;
                })->addIndexColumn()
                ->make(true);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->pageTitle = "All Books";
        $this->function = __FUNCTION__;
        $mainCategory = BookCategory::whereNull("parent_id")->get();

        $data = ["mainCategory" => $mainCategory];

        return view("dashboard.main", compact("data"))->with(["pageTitle" => $this->pageTitle, "class" => $this->class, "function" => $this->function]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->function = __FUNCTION__;
        $this->pageTitle = "Create New Book";
        $authors = Author::all();
        $categories = BookCategory::all();

        $data = ["authors" => $authors, "categories" => $categories];

        return view("dashboard.main", compact("data"))->with(["pageTitle" => $this->pageTitle, "class" => $this->class, "function" => $this->function]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
            'name' => 'required',
            'author_id' => 'required'
        ]);

        if ($validator) {

            DB::beginTransaction();
            $attr = $request->only(app(Book::class)->getFillables());
            $result = Book::create($attr);
            $result->authors()->attach($request->input("author_id"));
            DB::commit();

            return redirect(route('book.index'))->with('result', 'Book Added successfully');
        } else {
            abort(500);
        }
    }

    public function createDetails($book_id)
    {
        $this->function = "book_details";
        $this->pageTitle = "Add book details";

        return view("dashboard.main")->with(["pageTitle" => $this->pageTitle, "class" => $this->class, "function" => $this->function, "book_id" => $book_id]);
    }

    public function storeDetails(Request $request)
    {
        if ($request->has("details")) {
            DB::beginTransaction();
            $data = $request->input("details");

            foreach ($data as $d) {
                $d["book_id"] = $request->input("book_id");
                BookDetail::create($d);
            }

            DB::commit();
            return redirect(route('book.index'))->with('result', 'Book Added successfully');

        } else {
            abort(422);
        }
    }

    public function show($book_id)
    {
        $this->function = "show";
        $this->pageTitle = "All book details";
        $bookDetails = BookDetail::where("book_id", $book_id)->get();
        $data = ["bookDetails" => $bookDetails];

        return view("dashboard.main", compact("data"))->with(["pageTitle" => $this->pageTitle, "class" => $this->class, "function" => $this->function, "book_id" => $book_id]);
    }


    public function getSubCategory(Request $request)
    {
        if ($request->has("main_category") && $request->input("main_category") != null) {
            $mainCategory = $request->input("main_category");
            $subCategory = BookCategory::where("parent_id", $mainCategory)->get();
            $content = '';

            if ($subCategory)
                foreach ($subCategory as $subctg)
                    $content .= "<option value=" . $subctg->id . ">" . $subctg->name . "</option>";

            $data['success'] = true;
            $data['result'] = $content;
        } else {
            $data['success'] = false;
            $data['result'] = '';
        }

        return response()->json($data);
    }
}
