<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Routing\Controller;

class VanillaJSController extends Controller
{
    public $function;
    public $class;
    public $pageTitle;

    public function __construct()
    {
        $this->class = 'vanilla_js';
    }


    public function index()
    {
        $this->pageTitle = "All Question";
        $this->function = "index";

        return view("dashboard.main")->with(["pageTitle" => $this->pageTitle, "class" => $this->class, "function" => $this->function]);
    }
}
