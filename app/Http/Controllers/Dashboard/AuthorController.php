<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Author;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class AuthorController extends Controller
{
    public $function;
    public $class;
    public $pageTitle;

    /**
     * BookController constructor.
     */
    public function __construct()
    {
        $this->class = 'author';
        $this->searchValue = '';
    }


    public function dataTable(Request $request)
    {
        if ($request->ajax()) {

            $data = Author::orderBy("id", "ASC");

            return DataTables::eloquent($data)
                ->filterColumn('summary', function ($query, $keyword) {
                    if (!empty($keyword)) {
                        $data = $query->where('summary', 'LIKE', '%' . (string)$keyword . '%')->first();
                        if (isset($data) && checkVariable($data)) return true;
                    }
                    return false;
                })
                ->editColumn('summary', function ($row) {
                    return strip_tags($row->summary);
                })
                ->addIndexColumn()
                ->make(true);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->pageTitle = "All Authors";
        $this->function = __FUNCTION__;

        return view("dashboard.main")->with(["pageTitle" => $this->pageTitle, "class" => $this->class, "function" => $this->function]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->function = __FUNCTION__;
        $this->pageTitle = "Create New Author";
        $data = [];

        return view("dashboard.main", compact("data"))->with(["pageTitle" => $this->pageTitle, "class" => $this->class, "function" => $this->function]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
            'name' => 'required'
        ]);

        if ($validator) {

            DB::beginTransaction();
            $attr = $request->only(app(Author::class)->getFillables());
            $result = Author::create($attr);
            DB::commit();

            return redirect(route('author.index', $result->id))->with('result', 'Author Added successfully');
        } else {
            abort(500);
        }
    }
}
