<?php

namespace App\Http\Resources;

use App\Models\BookCategory;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $subCategory =[];
        if (is_null($this->parent_id))
            $subCategory = CategoryResource::collection(BookCategory::where('parent_id', $this->id)->get());

        return [
            'name' => $this->name,
            'subCategory' => $subCategory,
        ];

    }
}
