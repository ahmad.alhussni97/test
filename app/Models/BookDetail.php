<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use function Symfony\Component\Translation\t;

class BookDetail extends Model
{
    use HasFactory;

    protected $fillable=["page_id","content","book_id"];

    public static function getFillables()
    {
        return (new BookDetail())->fillable;
    }

    public function book(){

        return $this->belongsTo(Book::class,"book_id","id");

    }
}
