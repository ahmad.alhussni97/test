<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Book extends Model
{
    use HasFactory;

    protected $fillable=["name","summary","page_count","cat_id"];

    public static function getFillables()
    {
        return (new Book())->fillable;
    }

    public function bookCategory(){

        return $this->belongsTo(BookCategory::class,'cat_id', 'id');
    }

    public function bookDetails(){

        return $this->hasMany(BookDetail::class, 'book_id', 'id');
    }

    public function authors(){

        return $this->belongsToMany(Author::class,"authors_books", 'book_id','author_id');
    }
}
