<?php

namespace App\View\Components;

use Illuminate\View\Component;

class sideBar extends Component
{
    public $websiteInfo;
    public $userInfo;
    public $menu;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($websiteInfo,$userInfo,$menu)
    {
        $this->websiteInfo=$websiteInfo;
        $this->userInfo=$userInfo;
        $this->menu=$menu;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.dashboard.side-bar');
    }
}
