<?php

namespace App\View\Components;

use Illuminate\View\Component;

class CreateWidget extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */

    public $info;

    public function __construct($pageTitle,$role,$route,$address)
    {
        $this->pageTitle=$pageTitle;
        $this->role=$role;
        $this->route=$route;
        $this->address=$address;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.dashboard.create-widget');
    }
}
